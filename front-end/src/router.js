import Vue from 'vue'
import VueRouter from 'vue-router'

import ChatRoom from "./components/ChatRoom";
import StartForm from "./components/StartForm";

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'start-form',
            component: StartForm,
        },
        {
            path: '/chat',
            name: 'chat',
            component: ChatRoom,
            props: true
        }
    ]
})
