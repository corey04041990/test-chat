const express = require('express');
const socketIO = require('socket.io');
const http = require('http')
const users = require('./users')()

const port = process.env.PORT || 8081
const app = express()
const server = http.createServer(app)
const io = socketIO(server)

const message = (name, text, id) => ({name, text, id})

io.on('connection', socket => {

    socket.on('getName', (user, callback) => {
        callback({userId: socket.id})
        users.add(socket.id, user)

        io.emit('users:update', users.users)
        socket.emit('message:new', message('Admin', `Welcome, ${user}!`))
        socket.broadcast.emit('message:new', message('Admin', `${user} joined`))
    })

    socket.on('getMessage', data => {
        io.emit('message:new', message(data.user.name, data.value, data.user.id))

    })

    socket.on('disconnect', () => {
        const user = users.remove(socket.id)
        if (user) {
            io.emit('message:new', message('Admin', `${user.name}, left.`))
            io.emit('users:update', users.users)
        }
    })
})

server.listen(port, () => {
    console.log(`server running http://localhost:${port}`);
});
