class Users {
    constructor() {
        this.users = []
    }

    add(id, name) {
        this.users.push({
            id, name
        })
    }

    get(id) {
        return this.users.find(u => u.id === id)
    }

    remove(id) {
        const user = this.get(id)
        if (user) {
            this.users = this.users.filter(u => u.id !== user.id)
        }
        return user
    }
}

module.exports = function () {
    return new Users()
}
